package com.example.bindingtest;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

public class OTA {
    final private String TAG = "OTA";
    private Context context;
    private InputStream ins;
    private byte[][] firmwareSliced;
    private int fileSize, slicedNum, packetSentCount = 0;
    //private byte[] firmware;

    public OTA(Context context) throws IOException {
        this.context = context;
        ins = context.getResources().openRawResource(
                context.getResources().getIdentifier("ota",
                        "raw", context.getPackageName()));
        fileSize = ins.available();
        slicedNum = (fileSize+192-1)/192;
        firmwareSliced = new byte[slicedNum][203]; //203 = 2+4+4+192
        Log.i(TAG, "OTA file size : "+fileSize);
        slice(ins, 0);
        //firmware = new byte[fileSize];
        //ins.read(firmware);
    }

    private void slice(InputStream ins, int offset){
        if((fileSize-offset)/192>0){
            byte[] bytes = new byte[0];
            try {
                bytes = new byte[192];
                ins.read(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //String str = new String(bytes);
            byte[] command = new byte[2+4+4+192+1];
            command[0] = 0x68;
            command[1] = 0x01;
            for(int i = 0; i<4; i++)command[i+2] = (byte)((offset&(0xFF<<i*8))>>(i*8));
            //list.addAll(Arrays.asList(new byte[]{(byte)(offset&0xFF), (byte)(offset&0xFF00>>8), (byte)(offset&0xFF0000>>16), (byte)(offset&0xFF000000>>24)}));
            for(int i = 0; i<bytes.length; i++) command[i+10]=bytes[i];
            //command[10] = bytes[0];
            command[6] = (byte)192;
            command[7] = 0;
            command[8] = 0;
            command[9] = 0;
            command[command.length-1] = (byte)(sum(command));
            offset += 192;
            Log.i(TAG, (offset/192)+"");
            firmwareSliced[(offset/192)-1] = command;
            //list.addAll(Arrays.asList(new byte[]{(byte)0xc8, 0x00, 0x00, 0x00}));
            Log.d(TAG, "Command : "+byteArr2Str(firmwareSliced[(offset/192)-1]));
            Log.d(TAG, "Last Offset : "+(offset-192));
            Log.d(TAG, "Current Offset : "+offset);
            slice(ins, offset);
        }else if((fileSize-offset)%192>0){
            int len = fileSize-offset;
            Log.d(TAG, "Last Packet");
            Log.d(TAG, "Packet length : "+len);
            byte[] bytes = new byte[0];
            try {
                bytes = new byte[len];
                ins.read(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
            byte[] command = new byte[2+4+4+len+1];
            command[0] = 0x68;
            command[1] = 0x01;
            for(int i = 0; i<4; i++)command[i+2] = (byte)((offset&(0xFF<<i*8))>>(i*8));
            for(int i = 0; i<bytes.length; i++) command[i+10]=bytes[i];
            command[6] = (byte)len;
            command[7] = 0;
            command[8] = 0;
            command[9] = 0;
            command[command.length-1] = (byte)(sum(command));
            Log.i(TAG, firmwareSliced.length-1+"");
            offset += len;
            firmwareSliced[firmwareSliced.length-1] = command;
            Log.d(TAG, "Command : "+byteArr2Str(firmwareSliced[firmwareSliced.length-1]));
            Log.d(TAG, "Last Offset : "+(offset-len));
            Log.d(TAG, "Current Offset : "+offset);
            Log.i(TAG, "Finish");
        }else{
            Log.i(TAG, "Finish");
        }
    }

    private byte[][] getFirmwareSliced(){
        return firmwareSliced;
    }

    private int sum(byte[] list) {
        int sum = 0;
        for (byte i: list) {
            sum += i;
        }
        return sum;
    }

    private String byteArr2Str(byte[] data){
        String str = "";
        for(int i = 0; i < data.length; i++) str += (Integer.toHexString(data[i])+" ");
        return str.toUpperCase();
    }

    public byte[] getNextFrag(){
        Log.i(TAG, byteArr2Str(firmwareSliced[packetSentCount]));
        Log.i(TAG, "Packet count : "+packetSentCount);
        return firmwareSliced[packetSentCount++];
    }

    public byte[] getCRC(){
        ins = context.getResources().openRawResource(
                context.getResources().getIdentifier("ota",
                        "raw", context.getPackageName()));
        byte[] bytes = new byte[0];
        try {
            bytes = new byte[ins.available()];
            ins.read(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        CRC crc = new CRC();
        int crc16 = crc.getCrc(bytes);
        Log.d("TAG", "CRC 7 : "+byteArr2Str(new byte[] {(byte)(crc16&0xFF), (byte)((crc16&0xFF00)>>8)}));
        byte[] command = new byte[] {0x68, 0x06, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, (byte)(crc16&0xFF), (byte)((crc16&0xFF00)>>8), 0};
        command[command.length-1] = 0x55;//(byte) sum(command);
        //Log.d("TAG", "Command : " + byteArr2Str(command));
        return command;
    }


    public int getSlicedNum(){
        return slicedNum;
    }

    public int getPacketSentCount() {
        return packetSentCount;
    }
}