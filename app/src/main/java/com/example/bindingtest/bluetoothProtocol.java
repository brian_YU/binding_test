package com.example.bindingtest;

public class bluetoothProtocol {
    private final String TAG = "Bluettoht protocol";
    private final byte[] COMMAND_PREFIX = {'C', 'M', 'D'};
    private final byte[] RECEIVE_PREFIX = {'M', 'C', 'U'};
    private final byte[] OTA_PREFIX = {0x68};
    private final byte[] OTA_MCU_COMMAND = {0x01, 0x02, 0x03, 0x04, 0x06, 0x07};
    private final byte[] OTA_APP_COMMAND = {0x00, 0x05, 0x08, 0x08, 0x09};
    private final byte[] DATA_LENGTH_RECEIVE = {0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x00, 0x02, 0x03, 0x0C, 0x0C, 0x03, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00};

    public bluetoothProtocol(){

    }

    public enum Command {
        NOTHING((byte)0x00),
        RESET((byte)0x00),
        SET_LIGHT((byte)0x04),
        SET_HEATER((byte)0x04),
        SET_LOW_BATT_ALARM((byte)0x02),
        REQ_BATTERY_LEVEL((byte)0x00), //Request battery level
        ACK_MCU_ALARM((byte)0x00),
        SET_VIBRATE((byte)0x01),
        REQ_LUX_VALUE((byte)0x00),
        REQ_TEMP_AND_HUMIDITY((byte)0x00),
        REQ_IMU_DATA((byte)0x01),
        REQ_MCU_SERIAL_NUMBER((byte)0x00),
        POWER_OFF((byte)0x00),
        REQ_STEPS((byte)0x00),
        SET_ALARM_DELAY_TIME((byte)0x01),
        REQ_ALARM_DELAY_TIME((byte)0x00),
        FACTORY_RESET((byte)0x00),
        SET_TIME((byte)0x06),
        SYN_STEPS((byte)0x00), //NO function
        REQ_USING_TIME((byte)0x00), //NO function
        SYN_USING_TIME((byte)0x00), //NO function
        INVALID_FUNCTION((byte)0x00), //NO function
        SET_SOS_BTN_TO_ON_OFF_MODE((byte)0x01),
        SET_BONDING_MAC_ADDRESS((byte)0x0C),
        RELEASE_BONDING((byte)0x01),
        REQ_ALL_SENSOR_DATA((byte)0x01),
        DISCONNECT((byte)0x00),
        REQ_NTC_AND_TOUCH_SENSOR((byte)0x00),
        SET_BLUETOOTH_ALAM((byte)0x01),
        REQ_CHARGING_STATE((byte)0x00),
        SET_RGB_TO_WHITE((byte)0x01);

        private final byte value;

        Command(byte value) {
            this.value = value;
        }
        public byte getValue() {
            return value;
        }

    }

    public enum OTAMCUCommand {
        NOTHING((byte)0x99),
        UPDATE_DATA((byte)0x01),
        COM_TEST((byte)0x02),
        DELETE_COMMAND((byte)0x03),
        RESTAET_COMMAND((byte)0x04),
        CRC_CHECKSUM((byte)0x06),
        CHECK_VERSION_INFO((byte)0x07);

        private final byte value;

        OTAMCUCommand(byte value) {
            this.value = value;
        }
        public byte getValue() {
            return value;
        }

    }

    public enum OTAAPPCommand{
        NOTHING((byte)0x99),
        COMFIRM_FRAME((byte)0x00),
        ERROR_FRAME((byte)0x05),
        VERSION_INFO((byte)0x05),
        CRC_CHECKSUM_FRAME((byte)0x08),
        TIMEOUT_ERROR_FRAME((byte)0x09);

        private final byte value;

        OTAAPPCommand(byte value) {
            this.value = value;
        }
        public byte getValue() {
            return value;
        }
    }

    public String[] description = {
            "Nothing",
            "Reset",
            "Set Light",
            "Set Heat",
            "Set Low Battery Alarm",
            "Request Battery Level",
            "Acknowledge MCU Alarm",
            "Set Vibrate",
            "Request Lux Value",
            "Request Temperature and Humidity",
            "Request IMU Data",
            "Request MCU Serial Number",
            "Power Off",
            "Request Steps",
            "Set Alarm Delay Time",
            "Request Alarm Delay Time",
            "Factory Reset",
            "Set Time",
            "Synchronize Steps", //NO function
            "Request Using Time", //NO function
            "Synchronize Using Time", //NO function
            "Invalid Function", //NO function
            "Set SOS Button to On/Off Mode",
            "Set Bonding MAC Address",
            "Release Bonding",
            "Request all sensor data",
            "Disconnected",
            "Request NTC and Touch sensor value",
            "Set Bluetooth disconnect alarm",
            "Request charging state",
            "Set RGB to white"
    };

    public String[] OTAMCUDescription = {
            "Nothing",
            "Update data",
            "Communication Testing",
            "Delete Command",
            "Restart Command",
            "CRC Checksum",
            "Check Version Information"
    };

    public String[] OTAAPPDescription = {
            "Nothing",
            "Confirmation Frame",
            "Error Frame",
            "Version Information",
            "CRC Checksum Information",
            "Timeout Error Frame",
    };

    private bluetoothProtocol(Command cm) {
    }

    private byte[] checksum(byte[] command){
        byte sum = 0;
        for(int i = 0; i < command.length; i++)
            sum += command[i];
        return combine(command, new byte[]{sum});
    }

    private byte[] combine(byte[] a, byte[] b){
        int length = a.length + b.length;
        byte[] result = new byte[length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    public byte[] getCommand(final Command cm, final byte[] data){
//        return checksum(combine(combine(combine(COMMAND_PREFIX, new byte[]{DATA_LENGTH_COMMAND[cm.ordinal()]}), new byte[]{(byte) (cm.ordinal() + 1)}), data));
        return checksum(combine(combine(combine(COMMAND_PREFIX, new byte[]{cm.getValue()}), new byte[]{(byte) (cm.ordinal())}), data));
    }

    public String getCommandDescription(final Command cm){
        return description[cm.ordinal()];
    }

    public byte[] getOTAAPPCommand(final OTAAPPCommand cm, final byte[] data){
        return checksum(combine(combine(OTA_PREFIX, new byte[]{cm.getValue()}), data));
    }

    public String getOTAAPPCommandDesciption(final OTAAPPCommand cm){
        return OTAAPPDescription[cm.ordinal()];
    }

    public byte[] getOTAMCUCommand(final OTAMCUCommand cm, final byte[] data){
        return checksum(combine(combine(OTA_PREFIX, new byte[]{cm.getValue()}), data));
    }

    public String getOTAMCUCommandDesciption(final OTAMCUCommand cm){
        return OTAMCUDescription[cm.ordinal()];
    }

//    public byte[] getCommand(final int cm, final byte[] data){
//        return checksum(combine(combine(combine(COMMAND_PREFIX, new byte[]{DATA_LENGTH_COMMAND[cm]}), new byte[]{(byte) (cm + 1)}), data));
//    }


    public byte[] specialCommand(final byte[] prefix, final byte[] cm, final byte[] data){
        return checksum(combine(combine(prefix, cm), data));
    }

    public String byteArr2Str(byte[] data){
        String str = "";
        for(int i = 0; i < data.length; i++) str += (Integer.toHexString(data[i])+" ");
        return str.toUpperCase();
    }

    public byte getCommandNum(Command cmd){
        return (byte) (cmd.ordinal());
    }

    public static void main(String arg[]){
        bluetoothProtocol BP = new bluetoothProtocol();
//        byte[] temp = new byte[]{BP.DATA_LENGTH[Command.RESET.ordinal()]};
//        for (int i=0;i<=25;i++){
//            OTAMCUCommand cm = OTAMCUCommand.values()[i];
//            System.out.println("OLD " + Command.values()[i] + " : " + BP.byteArr2Str(BP.getCommand(cm, new byte[]{0x01})));
//            System.out.println("NEW " + Command.values()[i] + " : " + BP.byteArr2Str(BP.getCommand_new(cm, new byte[]{0x01})));
//            System.out.println(cm + "(" + cm.ordinal() + ")" + " : " +BP.getCommandDescription(cm));
//            System.out.println(cm + "(" + cm.ordinal() + ")" + " : " +BP.getOTAMCUCommandDesciption(cm));
//        }
//                    System.out.println(BP.byteArr2Str(BP.getOTAMCUCommand(OTAMCUCommand.CHECK_VERSION_INFO, new byte[]{})));
//        System.out.println();
//        System.out.println(BP.OTAMCUDescription[OTAMCUCommand.CHECK_VERSION_INFO.ordinal()]);
//        System.out.println(BP.getCommandNum(Command.SET_VIBRATE));
//        System.out.println(Command.values()[0]);
    }
}
