package com.example.bindingtest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.HashMap;

public class TestActivity extends AppCompatActivity {

    private static final String TAG = "TestActivity";

    boolean isBound = false;

    public static bluetooth bluetoothService;

    BluetoothGattCharacteristic characteristic;

    public static String[] tv = new String[]{"NA","NA","NA","NA","NA"};

    String MAC = "";

    static TextView connectionState;
    static TextView stickMac;
    TextView mac_tv;
    TextView tv_version;
    static TextView tv_ntc;
    String serial = "";


    private Handler handler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        tv[0] = "NA";tv[1] = "NA";tv[2] = "NA";tv[3] = "NA";tv[4] = "NA";

        mac_tv = findViewById(R.id.bind_text);
        connectionState = findViewById(R.id.connection_state);
        stickMac = findViewById(R.id.stick_mac);

        Intent intent = getIntent();
        MAC = intent.getStringExtra("MAC").split("-")[1];

        Intent startIntent = new Intent(TestActivity.this, bluetooth.class);
        startService(startIntent);
        bindService(startIntent,conn, Context.BIND_AUTO_CREATE);

        Log.d(TAG, MAC);

        //bluetoothService.setMac(MAC);

        for(int i = 0; i < 6; i++){
            if(Integer.parseInt(MAC.split(":")[i], 16) < 100) serial += "0";
            if(Integer.parseInt(MAC.split(":")[i], 16) < 10) serial += "0";
            serial += Integer.parseInt(MAC.split(":")[i], 16);
        }

        mac_tv.setText(MAC+"\n"+serial);



        this.registerReceiver(mConnReceiver,
                new IntentFilter("TEST_APP_SET_TEXT"));

    }

    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            isBound = true;
            bluetooth.LocalBinder myBinder = (bluetooth.LocalBinder)binder;
            bluetoothService = myBinder.getService();
            bluetoothService.setMac(MAC);
            Log.i(TAG, "ActivityA onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
            Log.i("DemoLog", "ActivityA onServiceDisconnected");
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        unbindService(conn);
        unregisterReceiver(mConnReceiver);
        //handler.removeCallbacks(runnable);
    }

    private String byteArr2Str(byte[] data){
        String str = "";
        for(int i = 0; i < data.length; i++) str += (Integer.toHexString(data[i])+" ");
        return str.toUpperCase();
    }

    private BroadcastReceiver mConnReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setTv(intent.getStringExtra("Content"), intent.getIntExtra("ID", 0));
            Log.d("Broadcast", "received " + intent.getStringExtra("Content"));
        }
    };

    public void setTv(String value, int i) {
        switch (i){
            case 0:
                connectionState.setText(value);
                break;
            case 1:
                stickMac.setText(value);
                break;
            case 2:
                Intent intent = new Intent(TestActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}
