package com.example.bindingtest;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.example.bindingtest.bluetoothProtocol.Command;

import static android.bluetooth.BluetoothAdapter.STATE_CONNECTED;
import static android.bluetooth.BluetoothGatt.GATT_SUCCESS;
import static android.telecom.Connection.STATE_DISCONNECTED;


public class bluetooth extends Service {

    public static final String TAG = "Bluetooth service";

    private final IBinder mBinder = new LocalBinder();

    UUID SERVICE_UUID = UUID.fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");
    UUID CONTROL_POINT_CHAR_UUID = UUID.fromString("6E400002-B5A3-F393-E0A9-E50E24DCCA9E");
    UUID COMMAND_RX_CHAR_UUID = UUID.fromString("6E400003-B5A3-F393-E0A9-E50E24DCCA9E");

    public static boolean connectStatus = false;
    public static int battPercentage = 0;
    public static String MAC = "NA";
    private Boolean manualStop = false;
    public static String firmwareVersion = "ERROR";
    public static int OTAProgress = 0;
    private int otaStage = 0;
    private OTA ota;


    private NotificationManager notificationManager;
    private BluetoothAdapter btAdapter;
    private BluetoothLeScanner scanner;
    private ScanSettings setting;
    private ArrayList<ScanFilter> filters;
    private BluetoothGatt bluetoothGatt;

    private ExecutorService singleThreadExecutor;

    private bluetoothProtocol BP;

    private BluetoothGattCharacteristic local_characteristic;

    @Override
    public void onCreate() {
        super.onCreate();

        bluetoothInit();
        receiverInit();
        notificationInit();

        showNotification(getString(R.string.notification_disconnect_title),getString(R.string.notification_noconnect_content),1);
        //showNotification(getString(R.string.notification_disconnect_title),getString(R.string.notification_noconnect_content),1);

        Log.d(TAG, "Pre device Mac: " + MAC);
        if (!MAC.equals("NA") && MAC != null) {
            setBTFilter(MAC);
            //startBTScan();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand() executed");

//        String command = null;
//
//        try {
//            command = intent.getStringExtra("command");
//        } catch (NullPointerException e) {
//            Log.e(TAG, "onStartCommand: " + e.getMessage());
//        }
//
//        if (command == null) {
//            Log.d(TAG, "onStartCommand() null");
//        }
//        else if (command.equals(String.valueOf(Command.SET_VIBRATE))) {
//            sendCommand(Command.SET_VIBRATE, new byte[]{0x01});
//
//            try {
//                Thread.sleep(2000);
//                sendCommand(Command.SET_VIBRATE, new byte[]{0x00});
//            }
//            catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//        else if (command.equals(String.valueOf(Command.POWER_OFF))) {
//            sendCommand(11);
//        }
//        else if (command.equals(String.valueOf(Command.RELEASE_BONDING))) {
//            sendUnbindCommand(bluetoothProtocol.Command.RELEASE_BONDING, new byte[]{0x01});
//        }
//        else if (command.equals("MACADDRESS")) {
//            if (intent.getStringExtra("MAC") != null)
//                setMac(intent.getStringExtra("MAC"));
//        }
//        else if (command.equals("SENSOR")) {
//            sendCommand(51);
//        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void bluetoothInit() {
        BluetoothManager btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();

        scanner = btAdapter.getBluetoothLeScanner();
        singleThreadExecutor = Executors.newSingleThreadExecutor();
        BP = new bluetoothProtocol();
    }

    private void receiverInit() {
        IntentFilter btFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(bluetoothOnOffBroadcastReceiver,btFilter);
    }

    private void notificationInit() {

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel notificationChannel =
                    new NotificationChannel("TestApp","TestAppNotification",
                            NotificationManager.IMPORTANCE_HIGH);

            notificationChannel.enableVibration(true);
            notificationChannel.enableLights(true);

            notificationManager.createNotificationChannel(notificationChannel);
        }

    }

    private void showNotification(String title, String content, int id) {


        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setChannelId("TestApp");

//        if (id == 1) { // onging
//            builder.setOngoing(true);
//        }
        notificationManager.notify(id, builder.build());

        if (id == 1) {
            startForeground(1, builder.build());
        }
    }

    public void setMac(String Mac){
        manualStop = true;
        this.MAC = Mac;
        if(bluetoothGatt != null)bluetoothGatt.close();
        setBTFilter(Mac);
        //startBTScan();
    }

    private String byteArr2Str(byte[] data){
        String str = "";
        for(int i = 0; i < data.length; i++) str += (Integer.toHexString(data[i])+" ");
        return str.toUpperCase();
    }

    public int UnsighedByte (byte data){
        return data&0x0FF ;
    }
    private void stickTurnOn() {

        if (!connectStatus) {
            if (MAC != null && !MAC.equals("NA")) {
                setBTFilter(MAC);
                //startBTScan();
            }
        }
    }

    private void stickTurnOff() {

        manualStop = true;
        connectStatus = false;
        if(bluetoothGatt != null) {
            bluetoothGatt.close();
            bluetoothGatt = null;
        }
        showNotification(getString(R.string.notification_disconnect_title),
                getString(R.string.notification_disconnect_content),1);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setBTFilter(String Mac){
        filters = new ArrayList<ScanFilter>();
        ScanFilter.Builder builder = new ScanFilter.Builder();
        //.setServiceUuid(ParcelUuid.fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E"))
        ScanFilter filter = builder.setDeviceName(Mac)
                //.setDeviceAddress(Mac)//.setServiceUuid(ParcelUuid.fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E"))
                .build();

        ScanSettings.Builder settingBuilder = new ScanSettings.Builder();
        settingBuilder.setCallbackType(ScanSettings.CALLBACK_TYPE_FIRST_MATCH)
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .setMatchMode(ScanSettings.MATCH_MODE_STICKY);

        setting = settingBuilder.setNumOfMatches(ScanSettings.MATCH_NUM_ONE_ADVERTISEMENT).build();
        filters.add(filter);
        startBTScan();
    }

    private void startBTScan() {
        try {
            scanner.stopScan(callBack);
            scanner.startScan(callBack);
        }
        catch (Exception e) {
            Log.d("startBTScan", e.getMessage());
        }
    }


    private ScanCallback callBack = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);

            //if (result.getDevice().getAddress() != null) //&& result.getDevice().getAddress() != "null")
                Log.d(TAG,"BLE device :"+
                        result.getDevice().getName() +
                        " : " + result.getDevice().getAddress()+
                        " : " + result.getRssi());
                if (result.getDevice().getAddress().equals(MAC)){
                    scanner.stopScan(callBack);
                    BluetoothDevice myDevice = result.getDevice();
                    bluetoothGatt = myDevice.connectGatt(bluetooth.this, true, gattCallback);
                }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
        }
    };


    private final BluetoothGattCallback gattCallback = new BluetoothGattCallback() {

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            local_characteristic = characteristic;
            Log.d(TAG,"gattCallback onCharacteristicChanged : "+ byteArr2Str(characteristic.getValue()));
            Log.d(TAG, "BLE device value: " + characteristic.getStringValue(0));
            Log.d(TAG, "BLE device hex value: " + byteArr2Str(characteristic.getValue()));
            if (characteristic.getValue()[0] != 0x68) {
                if (characteristic.getValue()[4] == BP.getCommandNum(Command.RESET)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.RESET));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.SET_LIGHT)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.SET_LIGHT));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.SET_HEATER)) {
                    sendCommand(Command.REQ_LUX_VALUE, new byte[]{});
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.SET_HEATER));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.SET_LOW_BATT_ALARM)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.SET_LOW_BATT_ALARM));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.REQ_BATTERY_LEVEL)) {
                    battPercentage = characteristic.getValue()[5];
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.REQ_BATTERY_LEVEL));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.ACK_MCU_ALARM)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(bluetoothProtocol.Command.ACK_MCU_ALARM));
                    if (characteristic.getValue()[5] == 0x01 && characteristic.getValue()[6] == 0x01) {
                        //sendSms();
                        showNotification("STICKu", getString(R.string.notification_sms_content), 2);
                    }
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.SET_VIBRATE)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.SET_VIBRATE));

                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.REQ_LUX_VALUE)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.REQ_LUX_VALUE));
                    //sendBroadcastMessage(("光照度 : "+characteristic.getValue()[6]), 1);
                    sendCommand(52);

                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.REQ_TEMP_AND_HUMIDITY)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.REQ_TEMP_AND_HUMIDITY));
                    //sendBroadcastMessage(("溫度 : "+  characteristic.getValue()[6]), 2);
                    sendCommand(53);

                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.REQ_IMU_DATA)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.REQ_IMU_DATA));
//                    sendBroadcastMessage(("陀螺儀 : "+
//                            (characteristic.getValue()[6]) + "," +
//                            (characteristic.getValue()[8]) + "," +
//                            (characteristic.getValue()[10]) + "\n" +
//                            (characteristic.getValue()[12]) + "," +
//                            (characteristic.getValue()[14]) + ","+
//                            (characteristic.getValue()[16])), 0);
                    sendCommand(54);
                }else if (characteristic.getValue()[4] == BP.getCommandNum(Command.REQ_MCU_SERIAL_NUMBER)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.REQ_MCU_SERIAL_NUMBER));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.POWER_OFF)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.POWER_OFF));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.REQ_STEPS)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.REQ_STEPS));
                    Log.i(TAG, "Total steps : " + (UnsighedByte(characteristic.getValue()[5]) << 8 | UnsighedByte(characteristic.getValue()[6])
                            + UnsighedByte(characteristic.getValue()[7]) << 8 | UnsighedByte(characteristic.getValue()[8])
                            + UnsighedByte(characteristic.getValue()[9]) << 8 | UnsighedByte(characteristic.getValue()[10])));


                   battPercentage = UnsighedByte(characteristic.getValue()[11]);
                    Log.i(TAG, "Updated steps.");
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.SET_ALARM_DELAY_TIME)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.SET_ALARM_DELAY_TIME));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.REQ_ALARM_DELAY_TIME)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.REQ_ALARM_DELAY_TIME));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.FACTORY_RESET)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.FACTORY_RESET));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.SET_TIME)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.SET_TIME));
                    sendCommand(Command.REQ_STEPS, new byte[]{});
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.SYN_STEPS)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.SYN_STEPS));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.REQ_USING_TIME)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.REQ_USING_TIME));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.SYN_USING_TIME)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.SYN_USING_TIME));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.INVALID_FUNCTION)) {
                    //NO FUNCTION
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.SET_SOS_BTN_TO_ON_OFF_MODE)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.SET_SOS_BTN_TO_ON_OFF_MODE));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.SET_BONDING_MAC_ADDRESS)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.SET_BONDING_MAC_ADDRESS));
                    sendBroadcastMessage("Bonding success", 1);
                    sendCommand(bluetoothProtocol.OTAMCUCommand.CHECK_VERSION_INFO, new byte[]{});
                    //sendCommand(Command.SET_SOS_BTN_TO_ON_OFF_MODE,new byte[]{0x00});
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.RELEASE_BONDING)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.RELEASE_BONDING));
                } else if (characteristic.getValue()[4] == BP.getCommandNum(Command.DISCONNECT)) {
                    Log.v(TAG + " Description", BP.getCommandDescription(Command.DISCONNECT));
                    gatt.close();
                    manualStop = true;
                    showNotification(getString(R.string.notification_disconnect_title),
                            getString(R.string.notification_disconnect_content), 1);
                    sendBroadcastMessage("Dissconnected", 2);
                    stopSelf();
                }else if (characteristic.getValue()[4] ==  BP.getCommandNum(Command.REQ_NTC_AND_TOUCH_SENSOR)) {
                    //sendBroadcastMessage(("發熱片溫度 : "+characteristic.getValue()[5]), 3);
                    //sendBroadcastMessage(("接觸感應2 : "+characteristic.getValue()[7]), 4);
                }
            } else {
                if (characteristic.getValue()[1] == 0x00) {
                    Log.d(TAG, "OTA Stage : " + otaStage);
                    switch (otaStage) {
                        case 1:
                            Log.d(TAG, "Clean flash success");
                            try {
                                ota = new OTA(getApplicationContext());
                                sendCommand(13);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            otaStage = 2;
                            break;
                        case 2:
                            Log.d(TAG, "OTA sent");
                            sendCommand(13);
                            getOTAprogress();
                            break;
                        case 3:
                            Log.d(TAG, "Check CRC");
                            sendCommand(14);
                            getOTAprogress();
                        case 4:
                            Log.d(TAG, "Power Off");
                            sendCommand(15);
                    }
                } else if (characteristic.getValue()[1] == 0x07) {
                    firmwareVersion = "版本 : "+Integer.toHexString(characteristic.getValue()[4]) + "."
                            + Integer.toHexString(characteristic.getValue()[3]) + "."
                            + Integer.toHexString(characteristic.getValue()[2]);
                    //sendBroadcastMessage(firmwareVersion, 5);
                    sendCommand(Command.SET_TIME, setTimeData());
                    Log.i(TAG, firmwareVersion);
                }
            }
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            Log.d(TAG,"gattCallback onConnectionStateChange : newState = " + newState +
                    " STATE_CONNECTED = " + STATE_CONNECTED +
                    " STATE_DISCONNECTED = " + STATE_DISCONNECTED +
                    " status = " + status +
                    " GATT_SUCCESS = " + GATT_SUCCESS +
                    " manualStop = " + manualStop);

            if (status == GATT_SUCCESS) {
                if (newState == STATE_CONNECTED) {

                    //scanner.stopScan(callBack);
                    //bluetoothGatt.discoverServices();
                    bluetoothGatt.requestMtu(210);
                    manualStop = false;
                    Log.d(TAG, "gattCallback onConnectionStateChange : STATE_CONNECTED");

                } else if (newState == STATE_DISCONNECTED) {

                    showNotification(getString(R.string.notification_disconnect_title),
                            getString(R.string.notification_disconnect_content), 1);

                    if (manualStop) {
                        bluetoothGatt.close();
                        bluetoothGatt = null;
                        scanner.startScan( callBack);
                        //scanner.startScan(filters, setting, callBack);
                        //scanner.startScan( callBack);
                    }

                    connectStatus = false;
                    battPercentage = 0;

                    Log.d(TAG, "gattCallback onConnectionStateChange : STATE_DISCONNECTED " + manualStop.toString());
                }
            }
            else {
                showNotification(getString(R.string.notification_disconnect_title),
                        getString(R.string.notification_disconnect_content), 1);

                connectStatus = false;
                battPercentage = 0;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);

            Log.d(TAG, "gattCallback onServicesDiscovered : ");

            BluetoothGattCharacteristic characteristic =
                    gatt.getService(SERVICE_UUID)
                            .getCharacteristic(COMMAND_RX_CHAR_UUID);
            //gatt.getService(SERVICE_UUID).getUuid().toString();

            setCharacteristicNotification(characteristic, true);

            showNotification(getString(R.string.notification_connect_title),
                    getString(R.string.notification_connect_content), 1);

            connectStatus = true;
            String phone = Long.toString((long)(Math.random()*1000000000000.0));
            //Log.d(TAG, "Test number : " +(long)(Math.random()*1000000000000.0));
            //String phone = "000000000000";
            Log.i(TAG, "Phone number : " + phone);

            try {
                sendCommand(Command.SET_BONDING_MAC_ADDRESS, phone.getBytes());//new byte[]{'0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'});
            } catch (NullPointerException e) {
                Log.e("gattCallback onServicesDiscovered : ", e.getMessage());
            }
            sendBroadcastMessage("Connected", 0);

        }


        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);

            Log.d(TAG, "gattCallback onMtuChanged : MTU = " + mtu + " Status = " + status);

            if (status == GATT_SUCCESS) {
                Log.i(TAG, "gattCallback onMtuChanged : MTU = " + mtu + " Success");
                gatt.discoverServices();
            }
        }

    };


    public void sendCommand(final int commandNum) {
        singleThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                if (connectStatus){
                    Boolean result;
                    switch (commandNum) {

                        case 51:
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(new byte[]{0x43, 0x4d, 0x44, 0x00, 0x08, 0x55});
                            while(!bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID)));
                            break;

                        case 52:
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(new byte[]{0x43, 0x4d, 0x44, 0x00, 0x09, 0x55});
                            while(!bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID)));
                            break;

                        case 53:
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(new byte[]{0x43, 0x4d, 0x44, 0x00, 0x0a, 0x55});
                            while(!bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID)));
                            break;

                        case 54:
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(new byte[]{0x43, 0x4d, 0x44, 0x00, 0x1b, 0x55});
                            while(!bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID)));
                            break;

                        case 1: // Request Battery Level
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(BP.getCommand(Command.REQ_BATTERY_LEVEL, new byte[]{}));//new byte[]{0x43, 0x4d, 0x44, 0x00, 0x05, 0x55});
                            //Log.d(TAG, Boolean.toString(gatt.writeCharacteristic(gatt.getService(SERVICE_UUID)
                            //        .getCharacteristic(CONTROL_POINT_CHAR_UUID))));
                            while(!bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID)));
                            break;
                        case 2:// Set Heater Off
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(BP.getCommand(Command.SET_HEATER, new byte[]{0x00, 0x28}));//new byte[]{0x43, 0x4d, 0x44, 0x02, 0x03, 0x00, 0x28, 0x55});
                            Log.d(TAG, Boolean.toString(bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID))));
                            break;
                        case 3: //Set Heater On
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(BP.getCommand(Command.SET_HEATER, new byte[]{0x01, 0x28}));//new byte[]{0x43, 0x4d, 0x44, 0x02, 0x03, 0x01, 0x28, 0x55});
                            Log.d(TAG, Boolean.toString(bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID))));
                            break;
                        case 4: //Vibrate on

                            break;
                        case 5: //Vibrate off

                            break;
                        case 6:// Set Time
                            Date currentTime = Calendar.getInstance().getTime();
                            SimpleDateFormat mdformat = new SimpleDateFormat("yy/MM/dd/HH/mm/ss");
                            String timeArray[] = mdformat.format(currentTime).split("/");
                            byte timeByteArray[] = {Byte.parseByte(timeArray[0]), Byte.parseByte(timeArray[1]), Byte.parseByte(timeArray[2]), Byte.parseByte(timeArray[3]),
                                    Byte.parseByte(timeArray[4]), Byte.parseByte(timeArray[5])};
                            Log.i(TAG, "Current Time : " + mdformat.format(currentTime));
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(BP.getCommand(Command.SET_TIME, new byte[]{timeByteArray[0], timeByteArray[1], timeByteArray[2],
                                    timeByteArray[3], timeByteArray[4], timeByteArray[5], 0x55}));
                            //new byte[]{0x43, 0x4d, 0x44, 0x06, 0x11, timeByteArray[0], timeByteArray[1], timeByteArray[2], timeByteArray[3]
                            //, timeByteArray[4], timeByteArray[5], 0x55});
//                        Log.d(TAG,Boolean.toString(gatt.writeCharacteristic(gatt.getService(SERVICE_UUID)
//                                .getCharacteristic(CONTROL_POINT_CHAR_UUID))));
                            while (!bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID)));
                            Log.d(TAG,Boolean.toString(true));
                            break;
                        case 7: // SOS ON
                            Log.d(TAG,"SOS ON");
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(BP.getCommand(Command.SET_SOS_BTN_TO_ON_OFF_MODE, new byte[]{0x01}));//new byte[]{0x43, 0x4d, 0x44, 0x01, 0x16, 0x01, 0x55});
//                                Log.d(TAG,Boolean.toString(gatt.writeCharacteristic(gatt.getService(SERVICE_UUID)
//                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID))));
                            while (!bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID))) ;
                            Log.d(TAG,"SOS ON");
                            break;
                        case 8: // Request Steps
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(BP.getCommand(Command.REQ_STEPS, new byte[]{}));//new byte[]{0x43, 0x4d, 0x44, 0x00, 0x0d, 0x55});//BP.getCommand(Command.REQ_STEPS, new byte[]{0x0D, 0x55}));//
                            Log.d(TAG,Boolean.toString(bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID))));

                            while (!bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID))) ;
                            break;
                        case 9: // Set Alarm Delay Time
                            break;
                        case 10: // Set Light
//                                gatt.getService(SERVICE_UUID)
//                                        .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(BP.getCommand(Command.SET_LIGHT, new byte[]{(byte)userSetting.getLux()}));
//                                Log.d(TAG, Boolean.toString(gatt.writeCharacteristic(gatt.getService(SERVICE_UUID)
//                                        .getCharacteristic(CONTROL_POINT_CHAR_UUID))));
                            break;

                        case 11: // Power OFF
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(BP.getCommand(Command.POWER_OFF, new byte[]{}));//new byte[]{0x43, 0x4d, 0x44, 0x00, 0x0d, 0x55});//BP.getCommand(Command.REQ_STEPS, new byte[]{0x0D, 0x55}));//
                            Log.d(TAG,Boolean.toString(bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID))));
                            break;
                        case 12: //Clean OTA flash
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(BP.specialCommand(new byte[]{0x68}, new byte[]{0x03}, new byte[]{}));//new byte[]{0x43, 0x4d, 0x44, 0x00, 0x0d, 0x55});//BP.getCommand(Command.REQ_STEPS, new byte[]{0x0D, 0x55}));//
                            Log.d(TAG,Boolean.toString(bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID))));
                            otaStage = 1;
                            break;
                        case 13: //Send OTA Packets
                            Log.i(TAG, "Progress : "+ota.getPacketSentCount()+"/"+ota.getSlicedNum());
                            //Log.i(TAG, "OTA : "+byteArr2Str(ota.getNextFrag()));
                            if(ota.getPacketSentCount() < ota.getSlicedNum()-1) {
                                Log.d(TAG, "Gatt:"+bluetoothGatt.toString());
                                byte[] temp = ota.getNextFrag();
                                Log.i(TAG, "Length"+temp.length);
                                //Log.i(TAG, "OTA packet : "+byteArr2Str(ota.getNextFrag()));
                                bluetoothGatt.getService(SERVICE_UUID)
                                        .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(temp);
                                Log.d(TAG, Boolean.toString(bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                        .getCharacteristic(CONTROL_POINT_CHAR_UUID))));
                                otaStage = 2;
                            }else{
                                byte[] temp = ota.getNextFrag();
                                Log.i(TAG, "Length"+temp.length);
                                bluetoothGatt.getService(SERVICE_UUID)
                                        .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(temp);
                                Log.d(TAG, Boolean.toString(bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                        .getCharacteristic(CONTROL_POINT_CHAR_UUID))));
                                otaStage = 3;
                            }
                            break;
                        case 14:
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(ota.getCRC());
                            Log.d(TAG, Boolean.toString(bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID))));
                            otaStage = 4;
                            break;
                        case 15: //Power off after OTA
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(new byte[]{0x68, 0x04, 0x6C});
                            Log.d(TAG, Boolean.toString(bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID))));
                            //otaStage = 4;
                            break;
                        case 16: //Firmware version
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID).setValue(new byte[]{0x68, 0x07, 0x6F});
                            result = bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID));
                            Log.d(TAG, Boolean.toString(result));
                            Log.i(TAG, "Get Firmware Version");
//                                while(!result) sendCommand(16);
                            //otaStage = 4;
                            break;
                    }
                }
            }
        });
    }


    public void sendCommand(final Command commandNum, final byte[] data) {

        singleThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                if (connectStatus){
                    byte[] cmd = BP.getCommand(commandNum, data);
                    boolean result = false;
                    if(bluetoothGatt != null) {
                        try {
                            bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID)
                                    .setValue(cmd);
                            result = bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                    .getCharacteristic(CONTROL_POINT_CHAR_UUID));
                        }
                        catch (NullPointerException e) {
                            Log.e(TAG,e.getMessage());
                        }
                        Log.i(TAG, "Command : " + byteArr2Str(cmd));
                        Log.d(TAG, Boolean.toString(result));
                        Log.i(TAG, BP.description[commandNum.ordinal()]);
                    }
                    if(!result) sendCommand(commandNum,data);
                }
            }
        });
    }

    public void sendCommand(final bluetoothProtocol.OTAMCUCommand commandNum, final byte[] data) {
        singleThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                if (connectStatus) {
                    byte[] cmd = BP.getOTAMCUCommand(commandNum, data);
                    boolean result = false;
                    bluetoothGatt.getService(SERVICE_UUID)
                            .getCharacteristic(CONTROL_POINT_CHAR_UUID)
                            .setValue(cmd);
                    result = bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                            .getCharacteristic(CONTROL_POINT_CHAR_UUID));
                    Log.i(TAG, "Command : " + byteArr2Str(cmd));
                    Log.d(TAG, Boolean.toString(result));
                    Log.i(TAG, BP.OTAMCUDescription[commandNum.ordinal()]);
                    if (!result) sendCommand(commandNum, data);
                }
            }
        });
    }

    public BluetoothGattCharacteristic getCharacteristic(){
        return local_characteristic;
    }

    public void sendUnbindCommand(final Command commandNum, final byte[] data) {
        singleThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                if (connectStatus){
                    byte[] cmd = BP.getCommand(commandNum, data);
                    boolean result = false;
                    if(bluetoothGatt != null) {
                        bluetoothGatt.getService(SERVICE_UUID)
                                .getCharacteristic(CONTROL_POINT_CHAR_UUID)
                                .setValue(cmd);
                        result = bluetoothGatt.writeCharacteristic(bluetoothGatt.getService(SERVICE_UUID)
                                .getCharacteristic(CONTROL_POINT_CHAR_UUID));
                        if(result) {
                            stickTurnOff();
                            MAC = "NA";
                        }
                        Log.i(TAG, "Command : " + byteArr2Str(cmd));
                        Log.d(TAG, Boolean.toString(result));
                        Log.i(TAG, BP.description[commandNum.ordinal()]);
                    }
                    if(!result) sendCommand(commandNum,data);
                }
            }
        });
    }


    private void getOTAprogress(){
        if(ota != null)
            OTAProgress = (int)(((float)ota.getPacketSentCount()/(float)ota.getSlicedNum())*100f);
        else OTAProgress = 0;
    }

    public int getOtaStage(){
        return otaStage;
    }

    private byte[] setTimeData(){
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat mdformat = new SimpleDateFormat("yy/MM/dd/HH/mm/ss");
        String timeArray[] = mdformat.format(currentTime).split("/");
        Log.i(TAG, "Current Time : " + mdformat.format(currentTime));
        return new byte[]{Byte.parseByte(timeArray[0]), Byte.parseByte(timeArray[1]), Byte.parseByte(timeArray[2]), Byte.parseByte(timeArray[3]),
                Byte.parseByte(timeArray[4]), Byte.parseByte(timeArray[5])};
    }



    private BroadcastReceiver bluetoothOnOffBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String TAG = "bluetoothOnOffBroadcastReceiver";

            String action = intent.getAction();
            if (action != null && action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {

                switch (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {

                    case BluetoothAdapter.STATE_OFF:
                        Log.d(TAG,"STATE_OFF");
                        stickTurnOff();
                        break;

                    case BluetoothAdapter.STATE_ON:
                        Log.d(TAG,"STATE_ON");
                        stickTurnOn();
                        break;
                }
            }
        }
    };

    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled) {
        //Log.d(TAG, "Set MTU:"+gatt.requestMtu(210));
        if (btAdapter == null || bluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        boolean isEnableNotification =  bluetoothGatt.setCharacteristicNotification(characteristic, enabled);
        if(isEnableNotification) {
            List<BluetoothGattDescriptor> descriptorList = characteristic.getDescriptors();
            if(descriptorList != null && descriptorList.size() > 0) {
                for(BluetoothGattDescriptor descriptor : descriptorList) {
                    descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    bluetoothGatt.writeDescriptor(descriptor);
                }
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        Log.d(TAG, "onBlind");
        return mBinder;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        //RealmDB.deleteCache(this); TODO: CHECK
        Log.d(TAG, "onDestroy() executed");
        unregisterReceiver(bluetoothOnOffBroadcastReceiver);
        stopForeground(true);
        stickTurnOff();
        scanner.stopScan(callBack);
    }

    public class LocalBinder extends Binder {
        public bluetooth getService() {
            // Return this instance of LocalService so clients can call public methods
            return bluetooth.this;
        }
    }

    public boolean getConnectStatus(){
        return connectStatus;
    }

    private void sendBroadcastMessage(String content, int id) {
        Intent intent = new Intent("TEST_APP_SET_TEXT");
        intent.putExtra("Content", content);
        intent.putExtra("ID", id);
        sendBroadcast(intent);
        Log.d("Broadcast", "sent");
    }
}
