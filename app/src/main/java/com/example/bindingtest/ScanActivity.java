package com.example.bindingtest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;

import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.List;

import static com.journeyapps.barcodescanner.CaptureManager.resultIntent;

public class ScanActivity extends AppCompatActivity {
    private CaptureManager capture;
    private DecoratedBarcodeView barcodeScannerView;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_scan);

        handler = new Handler();

        barcodeScannerView = (DecoratedBarcodeView) findViewById(R.id.barcodeView);

        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        barcodeScannerView.decodeContinuous(callback);
    }

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(final BarcodeResult result) {
            Log.d("Result",result.getText());
            if(result.getText().toUpperCase().split("-")[0].equals("STICKU")) {
                barcodeScannerView.pause();
                BeepManager beepManager = new BeepManager(ScanActivity.this);
                beepManager.playBeepSoundAndVibrate();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        returnResult(result);
                    }
                });
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {

        }

    };

    void returnResult(BarcodeResult rawResult) {
        Intent intent = resultIntent(rawResult, "Camera");
        this.setResult(Activity.RESULT_OK, intent);
        closeAndFinish();
    }

    void closeAndFinish() {
        if(barcodeScannerView.getBarcodeView().isCameraClosed()) {
            finish();
        } else {
            barcodeScannerView.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        capture.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

}